package br.com.nutrination.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.nutrination.dto.AgendamentoDto;
import br.com.nutrination.entities.Agendamento;
import br.com.nutrination.entities.Usuario;
import br.com.nutrination.repositories.AgendamentoRepository;
import br.com.nutrination.repositories.UsuarioRepository;

public class AgendamentoService {

	private static final Logger log = Logger.getLogger(AgendamentoService.class.getName());
	private final AgendamentoRepository agendamentoRepository = new AgendamentoRepository();
	private final UsuarioRepository usuarioRepository = new UsuarioRepository();

	public String createAgendamento(AgendamentoDto agendamentoDto) {
		Usuario paciente = new Usuario();
		Usuario nutricionista = new Usuario();
		Agendamento agendamento = new Agendamento();

		try {
			nutricionista = usuarioRepository.findById(Usuario.class, agendamentoDto.getIdNutricionista());
			paciente = usuarioRepository.findById(Usuario.class, agendamentoDto.getIdPaciente());

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Falha na consulta de Nutricionista ou paciente inválido");
			return "Falha na consulta de Nutricionista ou paciente inválido";
		}

		if (nutricionista != null && paciente != null) {
			agendamento.setNutricionista(nutricionista);
			agendamento.setPaciente(paciente);
			agendamento.setData(agendamentoDto.getData());
			agendamento.setHora(agendamentoDto.getHora());
			
			agendamentoRepository.saveOrUpdate(agendamento);
		} else {
			log.info("tem coisa nula");
			return "Falha ao salvar";
		}

		return "Agendamento criado com sucesso";
	}
	
	public List<Agendamento> findAllAgendamentos(String idNutricionista){
		List<Agendamento> agendamentos = new ArrayList<>();
		
		agendamentos = agendamentoRepository.findAllAgendamento(idNutricionista);
		
		return agendamentos;
	}
	public List<Agendamento> findAgendamentoByPaciente(String idNutricionista, String idPaciente){
		List<Agendamento> agendamentos = new ArrayList<>();
		
		agendamentos = agendamentoRepository.findAgendamentoByPaciente(idNutricionista, idPaciente);
		
		return agendamentos;
	}
	
	

}
