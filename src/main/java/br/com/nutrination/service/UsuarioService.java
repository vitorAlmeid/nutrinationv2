package br.com.nutrination.service;

import br.com.nutrination.dto.RetornoLoginDto;
import br.com.nutrination.dto.UsuarioLoginDto;
import br.com.nutrination.entities.Usuario;
import br.com.nutrination.repositories.UsuarioRepository;

public class UsuarioService {
	
	private final UsuarioRepository usuarioRepository = new UsuarioRepository();
	
	public RetornoLoginDto login(UsuarioLoginDto usuarioLoginDto) {
		RetornoLoginDto retornoLoginDto = new RetornoLoginDto();
		 
		
		try {
			Usuario usuario = usuarioRepository.login(usuarioLoginDto);
			if(usuario != null) {
				retornoLoginDto.setUsuario(usuario);
				retornoLoginDto.setMessage("Login efetuado com sucesso!");
				retornoLoginDto.setError(false);
			} else {
				retornoLoginDto.setUsuario(null);
				retornoLoginDto.setMessage("Não foi possível fazer login, senha ou login errado");
				retornoLoginDto.setError(false);
			}
			
		} catch (Exception e) {
			retornoLoginDto.setUsuario(null);
			retornoLoginDto.setMessage("DEU PAU");
			retornoLoginDto.setError(true);
		}
		return retornoLoginDto;
	}

}
