package br.com.nutrination.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.nutrination.dto.AvaliacaoDto;
import br.com.nutrination.entities.Avaliacao;
import br.com.nutrination.entities.Usuario;
import br.com.nutrination.repositories.AvaliacaoRepository;
import br.com.nutrination.repositories.UsuarioRepository;

public class AvaliacaoService {

	private static final Logger log = Logger.getLogger(AvaliacaoService.class.getName());
	private AvaliacaoRepository avaliacaoRepository = new AvaliacaoRepository();
	private final UsuarioRepository usuarioRepository = new UsuarioRepository();
	
	public AvaliacaoService() {
	}
	
	public void createAvaliacao(AvaliacaoDto avaliacaoDto){
		Usuario paciente = new Usuario();
		Usuario nutricionista = new Usuario();

		try {
			nutricionista = usuarioRepository.findById(Usuario.class, avaliacaoDto.getIdNutricionista());
			paciente = usuarioRepository.findById(Usuario.class, avaliacaoDto.getIdPaciente());

		} catch (Exception e) {
			e.printStackTrace();
			log.info("Falha na consulta de Nutricionista ou paciente inválido");
		}
		
		Avaliacao avaliacao = new Avaliacao(avaliacaoDto.getPeso(), avaliacaoDto.getAltura(), avaliacaoDto.getGorduraCorporal(),avaliacaoDto.getImc() ,nutricionista, paciente,avaliacaoDto.getData(),avaliacaoDto.getHora());
		
		avaliacaoRepository.saveOrUpdate(avaliacao);
	}
	
	public List<Avaliacao> findAllAvaliacoes(String idNutricionista){
		List<Avaliacao> avaliacoes = new ArrayList<>();
		
		avaliacoes = avaliacaoRepository.findAllAvaliacao(idNutricionista);
		
		return avaliacoes;
	}
	public List<Avaliacao> findAvaliacaoByPaciente(String idNutricionista, String idPaciente){
		List<Avaliacao> avaliacoes = new ArrayList<>();
		
		avaliacoes = avaliacaoRepository.findAvaliacaoByPaciente(idNutricionista, idPaciente);
		
		return avaliacoes;
	}

}
