package br.com.nutrination.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import br.com.nutrination.dto.AlimentoDto;
import br.com.nutrination.dto.DietaDto;
import br.com.nutrination.dto.RefeicaoDto;
import br.com.nutrination.dto.RetornoDietasDto;
import br.com.nutrination.entities.Alimento;
import br.com.nutrination.entities.AlimentoRefeicao;
import br.com.nutrination.entities.Dieta;
import br.com.nutrination.entities.Refeicao;
import br.com.nutrination.repositories.AlimentoRepository;
import br.com.nutrination.repositories.DietaRepository;
import br.com.nutrination.repositories.RefeicaoRepository;

public class DietaService {

	private final DietaRepository dietaRepository = new DietaRepository();
	private final AlimentoRepository alimentoRepository = new AlimentoRepository();
	private final RefeicaoRepository refeicaoRepository = new RefeicaoRepository();

	public void createDieta(DietaDto dietaDto) {
		Dieta dieta = new Dieta();
		if(dietaDto.getIdDieta() != null) {
			dieta.setIdDieta(dietaDto.getIdDieta());
		}
		dieta.setNome(dietaDto.getNome());
		dieta.setLocalDateTime(dietaDto.getLocalDateTime());
		dieta.setIdNutricionista(dietaDto.getIdNutricionista());
		dieta.setIdPaciente(dietaDto.getIdPaciente());
		dieta.setLocalDateTime(LocalDateTime.now());
		
		dietaRepository.saveOrUpdate(dieta);
		
		List<Refeicao> refeicoes = new ArrayList<Refeicao>();
		for (RefeicaoDto refeicaoDto : dietaDto.getRefeicoes()) {
			System.out.println("ENTROU AQUI LOUCURA");
			Refeicao refeicao = new Refeicao();
			if(refeicaoDto.getIdRefeicao()!= null) {
				refeicao.setIdRefeicao(refeicaoDto.getIdRefeicao());
			}
			refeicao.setNome(refeicaoDto.getNome());
			refeicao.setDieta(dieta);
			
			List<AlimentoRefeicao> alimentos = new ArrayList<AlimentoRefeicao>();
			for (AlimentoDto alimentoDto : refeicaoDto.getAlimentos()) {
				System.out.println("AQUI TAMBÉM O LOUCO");
				Alimento alimento = alimentoRepository.findById(Alimento.class, alimentoDto.getIdAlimento());
				AlimentoRefeicao alimentoRefeicao = new AlimentoRefeicao();
				alimentoRefeicao.setAlimento(alimento);
				alimentoRefeicao.setRefeicao(refeicao);
				alimentoRefeicao.setQuantidade(1D);
				alimentos.add(alimentoRefeicao);
			}
			
			refeicao.setAlimentos(alimentos);
			refeicaoRepository.saveOrUpdate(refeicao);

			refeicoes.add(refeicao);
		}

		
	}

	public RetornoDietasDto getAllDietas() {
		RetornoDietasDto retornoDietasDto = new RetornoDietasDto();
		List<Dieta> dietas = new ArrayList<Dieta>();

		try {
			dietas = dietaRepository.buscarTodos();
			if (!dietas.isEmpty()) {
				retornoDietasDto.setDietas(dietas);
				retornoDietasDto.setMessage("Login efetuado com sucesso!");
				retornoDietasDto.setError(false);
			} else {
				retornoDietasDto.setDietas(null);
				retornoDietasDto.setMessage("houve um problema ao buscar as dietas");
				retornoDietasDto.setError(false);
			}

		} catch (Exception e) {
			retornoDietasDto.setDietas(null);
			retornoDietasDto.setMessage("DEU PAU");
			retornoDietasDto.setError(true);
		}
		return retornoDietasDto;
	}

	public RetornoDietasDto getAllByIdPacienteAndIdNutricionista(Long idNutricionista, Long idPaciente) {
		RetornoDietasDto retornoDietasDto = new RetornoDietasDto();
		List<Dieta> dietas = new ArrayList<Dieta>();

		try {
			dietas = dietaRepository.buscarTodosPorIdPacienteAndIdNutricionista(idNutricionista, idPaciente);
			if (!dietas.isEmpty()) {
				retornoDietasDto.setDietas(dietas);
				retornoDietasDto.setMessage("Login efetuado com sucesso!");
				retornoDietasDto.setError(false);
			} else {
				retornoDietasDto.setDietas(null);
				retornoDietasDto.setMessage("houve um problema ao buscar as dietas");
				retornoDietasDto.setError(false);
			}

		} catch (Exception e) {
			retornoDietasDto.setDietas(null);
			retornoDietasDto.setMessage("DEU PAU");
			retornoDietasDto.setError(true);
		}
		return retornoDietasDto;
	}
	
	public Dieta buscarUltimaPaciente(Long idPaciente) {
		
			return dietaRepository.buscarUltimaPaciente(idPaciente);

	}
	
	public RetornoDietasDto getAllByIdNutricionista(Long idNutricionista) {
		RetornoDietasDto retornoDietasDto = new RetornoDietasDto();
		List<Dieta> dietas = new ArrayList<Dieta>();

		try {
			dietas = dietaRepository.buscarTodosPorIdNutricionista(idNutricionista);
			if (!dietas.isEmpty()) {
				retornoDietasDto.setDietas(dietas);
				retornoDietasDto.setMessage("Login efetuado com sucesso!");
				retornoDietasDto.setError(false);
			} else {
				retornoDietasDto.setDietas(null);
				retornoDietasDto.setMessage("houve um problema ao buscar as dietas");
				retornoDietasDto.setError(false);
			}

		} catch (Exception e) {
			retornoDietasDto.setDietas(null);
			retornoDietasDto.setMessage("DEU PAU");
			retornoDietasDto.setError(true);
		}
		return retornoDietasDto;
	}

}
