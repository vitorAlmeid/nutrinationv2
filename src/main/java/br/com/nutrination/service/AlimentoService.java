package br.com.nutrination.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import br.com.nutrination.dto.AlimentoDto;
import br.com.nutrination.dto.AtributoDto;
import br.com.nutrination.dto.RetornoAlimentosDto;
import br.com.nutrination.entities.Alimento;
import br.com.nutrination.repositories.AlimentoRepository;
import br.com.nutrination.utils.Utils;

public class AlimentoService {
	
	private final AlimentoRepository alimentoRepository = new AlimentoRepository();

	public void createAlimento(List<AlimentoDto> alimentosDto) throws IllegalArgumentException, IllegalAccessException {
		List<Alimento> alimentos = new ArrayList<>();
		
		for (AlimentoDto alimentoDto : alimentosDto) {
			Alimento alimento = new Alimento();
			alimento.setNome(alimentoDto.getDescricao());
			alimento.setTipoAlimento(alimentoDto.getCategoria());
			for(AtributoDto atributoDto :alimentoDto.getattributes()) {
				List<Field> fields = new ArrayList<Field>(Arrays.asList(Alimento.class.getDeclaredFields()));
				for (Iterator<Field> it = fields.iterator(); it.hasNext();) {
					Field field = it.next();
					if (Double.class.isAssignableFrom(field.getType())) {
						field.setAccessible(true);
						if (atributoDto.getNome() != null  && Utils.unaccent(atributoDto.getNome()).equalsIgnoreCase(field.getName())) {
							if(atributoDto.getNome().equals("energia")) {
								field.set(alimento, atributoDto.getKcal());
								it.remove();
								break;
							}
							field.set(alimento, atributoDto.getQtd());
							it.remove();
							break;
						}
					}
				}
			}
			alimentos.add(alimento);
		}
		
		for(Alimento alimento: alimentos) {
			alimentoRepository.saveOrUpdate(alimento);
		}
	}
	
	
	public RetornoAlimentosDto getAllAlimentos() {
		RetornoAlimentosDto retornoAlimentosDto = new RetornoAlimentosDto();
		List<Alimento> alimentos = new ArrayList<Alimento>();
		
		try {
			alimentos = alimentoRepository.buscarTodos();
			if(!alimentos.isEmpty()) {
				retornoAlimentosDto.setAlimentos(alimentos);
				retornoAlimentosDto.setMessage("Login efetuado com sucesso!");
				retornoAlimentosDto.setError(false);
			} else {
				retornoAlimentosDto.setAlimentos(null);
				retornoAlimentosDto.setMessage("houve um problema ao buscar os alimentos");
				retornoAlimentosDto.setError(false);
			}
			
		} catch (Exception e) {
			retornoAlimentosDto.setAlimentos(null);
			retornoAlimentosDto.setMessage("DEU PAU");
			retornoAlimentosDto.setError(true);
		}
		return retornoAlimentosDto;
	}
}
