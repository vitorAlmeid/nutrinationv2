package br.com.nutrination.dto;

import br.com.nutrination.enums.PerfilEnum;


public class UsuarioDto {
	
	private Long idUsuario;
	private String email;
	private String senha;
	private PerfilEnum permissao;
	private String nome;
	private String cpf;
	private String telefone;
	private String celular;
	private String rua;
	private	String numero;
	private String cep;
	private String bairro;
	private String cidade;
	private String estado;
	private String crnEstado;
	private Long child;
	
	public UsuarioDto(Long idUsuario, String email, String senha, PerfilEnum permissao, String nome, String cpf,
			String telefone, String celular, String rua, String numero, String cep, String bairro, String cidade, String estado,
			String crnEstado, Long child) {
		super();
		this.idUsuario = idUsuario;
		this.email = email;
		this.senha = senha;
		this.permissao = permissao;
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.celular = celular;
		this.rua = rua;
		this.numero = numero;
		this.cep = cep;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.crnEstado = crnEstado;
		this.child = child;
	}

	public UsuarioDto() {
		super();
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public PerfilEnum getPermissao() {
		return permissao;
	}

	public void setPermissao(PerfilEnum permissao) {
		this.permissao = permissao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCrnEstado() {
		return crnEstado;
	}

	public void setCrnEstado(String crnEstado) {
		this.crnEstado = crnEstado;
	}

	public Long getChild() {
		return child;
	}

	public void setChild(Long child) {
		this.child = child;
	}
	
	
			
}
