package br.com.nutrination.dto;

import br.com.nutrination.entities.Usuario;

public class RetornoLoginDto {
	
	private boolean error;
	private String message;
	private Usuario usuario;
	
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
