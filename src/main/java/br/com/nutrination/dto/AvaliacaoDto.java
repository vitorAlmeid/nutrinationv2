package br.com.nutrination.dto;

public class AvaliacaoDto {

    private double peso;
    private double altura;
    private double gorduraCorporal;
    private double imc;
    private String data;
    private String hora;
    private Long idNutricionista;
    private Long idPaciente;

	public AvaliacaoDto() {

	}

	public AvaliacaoDto(double peso, double altura, double gorduraCorporal, double imc, String dataAvaliacao,
			Long idPaciente, Long idNutricionista, String data, String hora) {
		super();
		this.peso = peso;
		this.altura = altura;
		this.gorduraCorporal = gorduraCorporal;
		this.imc = imc;
		this.idPaciente = idPaciente;
		this.idNutricionista = idNutricionista;
		this.data = data;
		this.hora = hora;
		
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getGorduraCorporal() {
		return gorduraCorporal;
	}

	public void setGorduraCorporal(double gorduraCorporal) {
		this.gorduraCorporal = gorduraCorporal;
	}

	public double getImc() {
		return imc;
	}

	public void setImc(double imc) {
		this.imc = imc;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Long getIdNutricionista() {
		return idNutricionista;
	}

	public void setIdNutricionista(Long idNutricionista) {
		this.idNutricionista = idNutricionista;
	}
	
	
	
	
}
