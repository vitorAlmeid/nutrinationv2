package br.com.nutrination.dto;

import java.util.List;

public class DietaDto {

	private Long idDieta;

	private String nome;
	private java.time.LocalDateTime localDateTime;
	private List<RefeicaoDto> refeicoes;
	private Long idNutricionista;
	private Long idPaciente;

	public Long getIdDieta() {
		return idDieta;
	}

	public void setIdDieta(Long idDieta) {
		this.idDieta = idDieta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public java.time.LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(java.time.LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}

	public List<RefeicaoDto> getRefeicoes() {
		return refeicoes;
	}

	public void setRefeicoes(List<RefeicaoDto> refeicoes) {
		this.refeicoes = refeicoes;
	}

	public Long getIdNutricionista() {
		return idNutricionista;
	}

	public void setIdNutricionista(Long idNutricionista) {
		this.idNutricionista = idNutricionista;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

}
