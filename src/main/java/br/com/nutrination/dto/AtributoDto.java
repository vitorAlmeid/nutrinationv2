package br.com.nutrination.dto;

public class AtributoDto {
	
	private String nome;
	private Double qtd;
	private String unidade;
	private Double kcal;
	private Double kj;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getQtd() {
		return qtd;
	}
	public void setQtd(Double qtd) {
		this.qtd = qtd;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public Double getKcal() {
		return kcal;
	}
	public void setKcal(Double kcal) {
		this.kcal = kcal;
	}
	public Double getKj() {
		return kj;
	}
	public void setKj(Double kj) {
		this.kj = kj;
	}

	
}
