package br.com.nutrination.dto;

public class AlimentoPuroDto {

	private Long idAlimento;
	private String nome;
	private String tipoAlimento;
	private Double proteinaPorCem;
	private Double carboidratoPorCem;
	private Double gorduraPorCem;
	private Double energia;
	private Double fibra;
	private Double fosforo;
	private Double potassio;
	private Double calcio;
	private Double magnesio;
	private Double cobre;
	private Double ferro;
	private Double manganes;
	private Double zinco;
	
	public Long getIdAlimento() {
		return idAlimento;
	}
	public void setIdAlimento(Long idAlimento) {
		this.idAlimento = idAlimento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipoAlimento() {
		return tipoAlimento;
	}
	public void setTipoAlimento(String tipoAlimento) {
		this.tipoAlimento = tipoAlimento;
	}
	public Double getProteinaPorCem() {
		return proteinaPorCem;
	}
	public void setProteinaPorCem(Double proteinaPorCem) {
		this.proteinaPorCem = proteinaPorCem;
	}
	public Double getCarboidratoPorCem() {
		return carboidratoPorCem;
	}
	public void setCarboidratoPorCem(Double carboidratoPorCem) {
		this.carboidratoPorCem = carboidratoPorCem;
	}
	public Double getGorduraPorCem() {
		return gorduraPorCem;
	}
	public void setGorduraPorCem(Double gorduraPorCem) {
		this.gorduraPorCem = gorduraPorCem;
	}
	public Double getEnergia() {
		return energia;
	}
	public void setEnergia(Double energia) {
		this.energia = energia;
	}
	public Double getFibra() {
		return fibra;
	}
	public void setFibra(Double fibra) {
		this.fibra = fibra;
	}
	public Double getFosforo() {
		return fosforo;
	}
	public void setFosforo(Double fosforo) {
		this.fosforo = fosforo;
	}
	public Double getPotassio() {
		return potassio;
	}
	public void setPotassio(Double potassio) {
		this.potassio = potassio;
	}
	public Double getCalcio() {
		return calcio;
	}
	public void setCalcio(Double calcio) {
		this.calcio = calcio;
	}
	public Double getMagnesio() {
		return magnesio;
	}
	public void setMagnesio(Double magnesio) {
		this.magnesio = magnesio;
	}
	public Double getCobre() {
		return cobre;
	}
	public void setCobre(Double cobre) {
		this.cobre = cobre;
	}
	public Double getFerro() {
		return ferro;
	}
	public void setFerro(Double ferro) {
		this.ferro = ferro;
	}
	public Double getManganes() {
		return manganes;
	}
	public void setManganes(Double manganes) {
		this.manganes = manganes;
	}
	public Double getZinco() {
		return zinco;
	}
	public void setZinco(Double zinco) {
		this.zinco = zinco;
	}
	
	
	
	
}
