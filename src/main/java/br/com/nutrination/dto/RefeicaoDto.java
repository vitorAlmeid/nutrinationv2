package br.com.nutrination.dto;

import java.util.List;

public class RefeicaoDto {

	private Long idRefeicao;
	
	private String nome;

	private List<AlimentoDto> alimentos;
	
	private Double quantidade;
	
	public Long getIdRefeicao() {
		return idRefeicao;
	}

	public void setIdRefeicao(Long idRefeicao) {
		this.idRefeicao = idRefeicao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<AlimentoDto> getAlimentos() {
		return alimentos;
	}

	public void setAlimentos(List<AlimentoDto> alimentos) {
		this.alimentos = alimentos;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	
	

}
