package br.com.nutrination.dto;


public class AgendamentoDto {
	
	private Long id;
	private Long idPaciente;
	private Long idNutricionista;
	private String data;
	private String hora;

	public AgendamentoDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Long getIdNutricionista() {
		return idNutricionista;
	}

	public void setIdNutricionista(Long idNutricionista) {
		this.idNutricionista = idNutricionista;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public AgendamentoDto(Long idPaciente, Long idNutricionista, String data, String hora) {
		super();
		this.idPaciente = idPaciente;
		this.idNutricionista = idNutricionista;
		this.data = data;
		this.hora = hora;
	}
	

}
