package br.com.nutrination.dto;

import java.util.List;

import br.com.nutrination.entities.Dieta;

public class RetornoDietasDto {

	private boolean error;
	private String message;
	private List<Dieta> dietas;
	
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;

	}
	public List<Dieta> getDieta() {
		return dietas;
	}
	public void setDietas(List<Dieta> dietas) {
		this.dietas = dietas;
	}
	
	
}