package br.com.nutrination.dto;

import java.util.List;

import br.com.nutrination.entities.Alimento;

public class RetornoAlimentosDto {

	private boolean error;
	private String message;
	private List<Alimento> alimentos;
	
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;

	}
	public List<Alimento> getAlimentos() {
		return alimentos;
	}
	public void setAlimentos(List<Alimento> alimentos) {
		this.alimentos = alimentos;
	}
	
	
}