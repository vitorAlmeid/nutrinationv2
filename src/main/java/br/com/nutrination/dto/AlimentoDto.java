package br.com.nutrination.dto;

import java.util.List;

public class AlimentoDto {

	private Long idAlimento;
	private String descricao;
	private int qtdBase;
	private String unidadeBase;
	private String categoria;
	private List<AtributoDto> attributes;

	public Long getIdAlimento() {
		return idAlimento;
	}

	public void setIdAlimento(Long idAlimento) {
		this.idAlimento = idAlimento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getQtdBase() {
		return qtdBase;
	}

	public void setQtdBase(int qtdBase) {
		this.qtdBase = qtdBase;
	}

	public String getUnidadeBase() {
		return unidadeBase;
	}

	public void setUnidadeBase(String unidadeBase) {
		this.unidadeBase = unidadeBase;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public List<AtributoDto> getattributes() {
		return attributes;
	}

	public void setattributes(List<AtributoDto> attributes) {
		this.attributes = attributes;
	}

}
