package br.com.nutrination.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.nutrination.dto.AlimentoDto;
import br.com.nutrination.dto.RetornoAlimentosDto;
import br.com.nutrination.service.AlimentoService;

@Path("/alimento")
public class AlimentoController {
	
	private AlimentoService alimentoService = new AlimentoService();
	
	@POST	
	@Consumes("application/json; charset=UTF-8; charset=UTF-8")
	@Produces("application/json; charset=UTF-8; charset=UTF-8")
	@Path("/cadastrar")
	public String cadastrar(List<AlimentoDto> alimentosDto){
		
		try {
			alimentoService.createAlimento(alimentosDto);
		} catch (Exception e) {
			e.printStackTrace();
			return "deu pau";
		}
		
		return "cadastrado com sucesso";
	}
	
	
	@GET
	@Consumes("application/json; charset=UTF-8; charset=UTF-8")
	@Produces("application/json; charset=UTF-8; charset=UTF-8")
	public RetornoAlimentosDto getAllAlimentos() {
		
		return alimentoService.getAllAlimentos();
	}

}
