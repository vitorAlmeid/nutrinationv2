package br.com.nutrination.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.nutrination.dto.AgendamentoDto;
import br.com.nutrination.entities.Agendamento;
import br.com.nutrination.service.AgendamentoService;

@Path("/agendamento")
public class AgendamentoController {

	public AgendamentoController() {
	}
	
	private AgendamentoService agendamentoService = new AgendamentoService();
	
	@POST	
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/cadastrar")
	public String cadastrar(AgendamentoDto agendamentoDto){
		
		try {
			agendamentoService.createAgendamento(agendamentoDto);
		} catch (Exception e) {
			e.printStackTrace();
			return "deu pau";
		}
		
		return "cadastrado com sucesso";
	}
	
	@GET	
	@Produces("application/json; charset=UTF-8")
	@Path("/{idNutricionista}")
	public List<Agendamento> findAll(@PathParam("idNutricionista")  String idNutricionista){
		List<Agendamento> agendamentos = new ArrayList<>();
		agendamentos = agendamentoService.findAllAgendamentos(idNutricionista);
		return agendamentos;
	}
	
	@GET	
	@Produces("application/json; charset=UTF-8")
	@Path("/{idNutricionista}/{idPaciente}")
	public List<Agendamento> findAgendamentoByPaciente(@PathParam("idNutricionista")  String idNutricionista, @PathParam("idPaciente")  String idPaciente){
		List<Agendamento> agendamentos = new ArrayList<>();
		agendamentos = agendamentoService.findAgendamentoByPaciente(idNutricionista, idPaciente);
		return agendamentos;
	}

}
