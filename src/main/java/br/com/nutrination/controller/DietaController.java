package br.com.nutrination.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.nutrination.dto.DietaDto;
import br.com.nutrination.dto.RetornoDietasDto;
import br.com.nutrination.entities.Dieta;
import br.com.nutrination.service.DietaService;

@Path("/dieta")
public class DietaController {
	private DietaService dietaService = new DietaService();

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/cadastrar")
	public String cadastrarDieta(DietaDto dietaDto) {

		try {
			dietaService.createDieta(dietaDto);
		} catch (Exception e) {
			e.printStackTrace();
			return "deu pau";
		}
		return "Cadastrado com sucesso!";
	}
	
	@GET
	@Consumes("application/json; charset=UTF-8; charset=UTF-8")
	@Produces("application/json; charset=UTF-8; charset=UTF-8")
	public RetornoDietasDto getAllDietas() {
		
		return dietaService.getAllDietas();
	}
	
	@GET
	@Consumes("application/json; charset=UTF-8; charset=UTF-8")
	@Produces("application/json; charset=UTF-8; charset=UTF-8")
	@Path("/{idNutricionista}/{idPaciente}")
	public RetornoDietasDto getAllByIdPacienteAndIdNutricionista(@PathParam("idNutricionista") Long idNutricionista, @PathParam("idPaciente") Long idPaciente) {
		return dietaService.getAllByIdPacienteAndIdNutricionista(idNutricionista, idPaciente);
	}
	
	@GET
	@Consumes("application/json; charset=UTF-8; charset=UTF-8")
	@Produces("application/json; charset=UTF-8; charset=UTF-8")
	@Path("/nutricionista/{idNutricionista}")
	public RetornoDietasDto getAllByIdPacienteAndIdNutricionista(@PathParam("idNutricionista") Long idNutricionista) {
		return dietaService.getAllByIdNutricionista(idNutricionista);
	}
	
	@GET
	@Consumes("application/json; charset=UTF-8; charset=UTF-8")
	@Produces("application/json; charset=UTF-8; charset=UTF-8")
	@Path("/paciente/{idPaciente}")
	public Dieta buscarUltimaPaciente(@PathParam("idPaciente") Long idPaciente) {
		return dietaService.buscarUltimaPaciente(idPaciente);
	}

}
