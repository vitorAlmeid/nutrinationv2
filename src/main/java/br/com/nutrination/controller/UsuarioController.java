package br.com.nutrination.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import br.com.nutrination.dto.RetornoLoginDto;
import br.com.nutrination.dto.UsuarioDto;
import br.com.nutrination.dto.UsuarioLoginDto;
import br.com.nutrination.entities.Usuario;
import br.com.nutrination.repositories.UsuarioRepository;
import br.com.nutrination.service.UsuarioService;

/**
 * Essa classe vai expor os nossos métodos para serem acessasdos via http
 * 
 * @Path - Caminho para a chamada da classe que vai representar o nosso serviço
 */
@Path("/usuario")
public class UsuarioController {

	private final UsuarioRepository usuarioRepository = new UsuarioRepository();
	private UsuarioService usuarioService = new UsuarioService();

	/**
	 * @Consumes - determina o formato dos dados que vamos postarnpm
	 * @Produces - determina o formato dos dados que vamos retornar
	 * 
	 *           Esse método cadastra um novo usuario
	 */

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/cadastrar")
	public String cadastrar(Usuario usuario) {

		try {
			usuarioRepository.saveOrUpdate(usuario);
			return "usuario salvo com sucesso";
		} catch (Exception e) {
			return "Erro ao cadastrar um registro " + e.getMessage();
		}
	}

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/login")
	public RetornoLoginDto login(UsuarioLoginDto usuarioLoginDto) {

		return usuarioService.login(usuarioLoginDto);

	}

	/**
	 * Esse método lista todos usuarios cadastrados na base
	 */
	@GET
	@Produces("application/json; charset=UTF-8")
	public List<UsuarioDto> todosUsuarios() {

		List<UsuarioDto> usuariosDto = new ArrayList<UsuarioDto>();

		List<Usuario> usuarios = usuarioRepository.buscarTodos();

		for (Usuario usuario : usuarios) {

			usuariosDto.add(new UsuarioDto(usuario.getIdUsuario(), usuario.getEmail(), usuario.getSenha(),
					usuario.getPermissao(), usuario.getNome(), usuario.getCpf(), usuario.getTelefone(),
					usuario.getCelular(), usuario.getRua(), usuario.getNumero(), usuario.getCep(), usuario.getBairro(),
					usuario.getCidade(), usuario.getEstado(), usuario.getCrnEstado(), usuario.getChild()));
		}

		return usuariosDto;
	}

	/**
	 * Esse método busca um usuario cadastrado pelo código
	 */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/{id}")
	public UsuarioDto findUsuario(@PathParam("id") Long idUsuario) {

		Usuario usuario = usuarioRepository.findById(Usuario.class, idUsuario);

		if (usuario != null) {
			return new UsuarioDto(usuario.getIdUsuario(), usuario.getEmail(), usuario.getSenha(),
					usuario.getPermissao(), usuario.getNome(), usuario.getCpf(), usuario.getTelefone(),
					usuario.getCelular(), usuario.getRua(), usuario.getNumero(), usuario.getCep(), usuario.getBairro(),
					usuario.getCidade(), usuario.getEstado(), usuario.getCrnEstado(), usuario.getChild());
		}

		return null;
	}
	
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/pacientes/{id}")
	public List<Usuario> findAllPacientes(@PathParam("id") Long idUsuario){
		
		return usuarioRepository.findAllPacientes(idUsuario);
		
	}
	

	/**
	 * Essse método altera um usuario já cadastrado
	 **/
	@PUT
	@Produces("application/json; charset=UTF-8")
	@Consumes("application/json; charset=UTF-8")
	@Path("/alterar")
	public String Alterar(UsuarioDto usuarioDto) {

		Usuario usuario = new Usuario();
		usuario.setCpf(usuarioDto.getCpf());
		usuario.setEmail(usuarioDto.getEmail());
		usuario.setSenha(usuarioDto.getSenha());
		usuario.setNome(usuarioDto.getNome());
		usuario.setPermissao(usuarioDto.getPermissao());
		usuario.setTelefone(usuarioDto.getTelefone());
		usuario.setIdUsuario(usuarioDto.getIdUsuario());
		usuario.setCelular(usuarioDto.getCelular());
		usuario.setRua(usuarioDto.getRua());
		usuario.setNumero(usuarioDto.getNumero());
		usuario.setCep(usuarioDto.getCep());
		usuario.setBairro(usuarioDto.getBairro());
		usuario.setCidade(usuarioDto.getCidade());
		usuario.setEstado(usuarioDto.getEstado());
		usuario.setCrnEstado(usuarioDto.getCrnEstado());

		try {
			usuarioRepository.saveOrUpdate(usuario);
			return "Registro alterado com sucesso!";

		} catch (Exception e) {
			return "Erro ao alterar o registro " + e.getMessage();
		}

	}

	/**
	 * Excluindo um usuario pelo código
	 */
	@DELETE
	@Produces("application/json; charset=UTF-8")
	@Path("/{id}/remover")
	public String excluir(@PathParam("id") Long idUsuario) {

		try {
			usuarioRepository.remove(Usuario.class, idUsuario);

			return "O registro de usuário foi excluido com sucesso!";
		} catch (Exception e) {
			return "Erro ao excluir o registro de usuário! " + e.getMessage();
		}
	}
}
