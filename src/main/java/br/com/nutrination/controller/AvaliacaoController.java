package br.com.nutrination.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.nutrination.dto.AvaliacaoDto;
import br.com.nutrination.entities.Avaliacao;
import br.com.nutrination.service.AvaliacaoService;

@Path("/avaliacao")
public class AvaliacaoController {

	public AvaliacaoController() {
	}

	private AvaliacaoService avaliacaoService = new AvaliacaoService();

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/cadastrar")
	public String cadastrarAvaliacao(AvaliacaoDto avaliacaoDto) {

		try {
			avaliacaoService.createAvaliacao(avaliacaoDto);
		} catch (Exception e) {
			e.printStackTrace();
			return "deu pau";
		}
		return "Cadastrado com sucesso!";

	}
	
	
	@GET	
	@Produces("application/json; charset=UTF-8")
	@Path("/{idNutricionista}")
	public List<Avaliacao> findAll(@PathParam("idNutricionista")  String idNutricionista){
		List<Avaliacao> avaliacoes = new ArrayList<>();
		avaliacoes = avaliacaoService.findAllAvaliacoes(idNutricionista);
		return avaliacoes;
	}
	
	@GET	
	@Produces("application/json; charset=UTF-8")
	@Path("/{idNutricionista}/{idPaciente}")
	public List<Avaliacao> findAgendamentoByPaciente(@PathParam("idNutricionista")  String idNutricionista, @PathParam("idPaciente")  String idPaciente){
		List<Avaliacao> avaliacoes = new ArrayList<>();
		avaliacoes = avaliacaoService.findAvaliacaoByPaciente(idNutricionista, idPaciente);
		return avaliacoes;
	}
}
