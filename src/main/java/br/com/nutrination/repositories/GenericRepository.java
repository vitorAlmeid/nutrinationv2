package br.com.nutrination.repositories;

import javax.persistence.EntityManager;

import br.com.nutrination.entities.GenericEntity;
import br.com.nutrination.utils.ConnectionFactory;

public class GenericRepository<T extends GenericEntity> {

	public GenericRepository() {

	}

	private static EntityManager entityManager = ConnectionFactory.getEntityManager();

	public T findById(Class<T> clazz, Long id) {
		return entityManager.find(clazz, id);
	}

	public void saveOrUpdate(T obj) {
		if(!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try {
			if (obj.getId() == null) {
				entityManager.persist(obj);
			} else {
				entityManager.merge(obj);
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Class<T> clazz, Long id) {
		entityManager = ConnectionFactory.getEntityManager();
		T t = findById(clazz, id);
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(t);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}

}
