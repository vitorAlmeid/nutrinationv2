package br.com.nutrination.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.nutrination.dto.UsuarioLoginDto;
import br.com.nutrination.entities.Usuario;
import br.com.nutrination.utils.ConnectionFactory;

public class UsuarioRepository extends GenericRepository<Usuario>{

	private static EntityManager entityManager = ConnectionFactory.getEntityManager();
	
	public UsuarioRepository() {
	}
	
	
	/*Cria um novo registro no banco de dados*/
	
	
	/**
	 * RETORNA TODOS OS USUARIOS CADASTRADOS NO BANCO DE DADOS 
	 * */
	@SuppressWarnings("unchecked")
	public List<Usuario> buscarTodos(){
		entityManager = ConnectionFactory.getEntityManager();
 
		return entityManager.createQuery("SELECT u FROM Usuario u order by u.nome").getResultList();
	}
	
	public Usuario findByEmail(String email) {
		entityManager = ConnectionFactory.getEntityManager();
		Query query = entityManager.createQuery("select u FROM Usuario u where u.email LIKE ?1 ").setParameter(1, email);
		return (Usuario) query.getSingleResult();
	}
	
	public Usuario login(UsuarioLoginDto usuarioLoginDto) {
		entityManager = ConnectionFactory.getEntityManager();
		Query query = entityManager.createQuery("select u FROM Usuario u where u.email LIKE ?1 and u.senha LIKE ?2").setParameter(1, usuarioLoginDto.getEmail());
		query.setParameter(2, usuarioLoginDto.getSenha());
		
		return (Usuario) query.getSingleResult();
	}
		
	@SuppressWarnings("unchecked")
	public List<Usuario> findAllPacientes(Long idNutricionista) {
		
		entityManager = ConnectionFactory.getEntityManager();
		Query query = entityManager.createQuery("select u FROM Usuario u where u.child LIKE ?1").setParameter(1, idNutricionista);
		
		return  query.getResultList();
	}
		

}
