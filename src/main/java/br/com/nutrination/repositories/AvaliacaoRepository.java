package br.com.nutrination.repositories;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.nutrination.entities.Avaliacao;
import br.com.nutrination.utils.ConnectionFactory;

public class AvaliacaoRepository extends GenericRepository<Avaliacao> {
	
	private static EntityManager entityManager = ConnectionFactory.getEntityManager();

	public AvaliacaoRepository() {
	}
	
	@SuppressWarnings("unchecked")
	public List<Avaliacao> findAllAvaliacao(String idNutricionista) {
		entityManager = ConnectionFactory.getEntityManager();
		return entityManager.createQuery("SELECT a FROM Avaliacao a where a.nutricionista = "+idNutricionista).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Avaliacao> findAvaliacaoByPaciente(String idNutricionista, String idPaciente) {
		entityManager = ConnectionFactory.getEntityManager();
		return entityManager.createQuery("SELECT a FROM Avaliacao a where a.nutricionista = "+idNutricionista +" and a.paciente = "+idPaciente).getResultList();
	}

}
