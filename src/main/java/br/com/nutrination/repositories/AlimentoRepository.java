package br.com.nutrination.repositories;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.nutrination.entities.Alimento;
import br.com.nutrination.utils.ConnectionFactory;

public class AlimentoRepository extends GenericRepository<Alimento> {
	
	private static EntityManager entityManager = ConnectionFactory.getEntityManager();

	@SuppressWarnings("unchecked")
	public List<Alimento> buscarTodos(){
		entityManager = ConnectionFactory.getEntityManager();
 
		return entityManager.createQuery("SELECT a FROM Alimento a order by a.nome").getResultList();
	}
}
