package br.com.nutrination.repositories;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.nutrination.entities.Agendamento;
import br.com.nutrination.utils.ConnectionFactory;

public class AgendamentoRepository extends GenericRepository<Agendamento> {
	
	private static EntityManager entityManager = ConnectionFactory.getEntityManager();

	public AgendamentoRepository() {
	}
	
	@SuppressWarnings("unchecked")
	public List<Agendamento> findAllAgendamento(String idNutricionista) {
		entityManager = ConnectionFactory.getEntityManager();
		return entityManager.createQuery("SELECT a FROM Agendamento a where a.nutricionista = "+idNutricionista).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Agendamento> findAgendamentoByPaciente(String idNutricionista, String idPaciente) {
		entityManager = ConnectionFactory.getEntityManager();
		return entityManager.createQuery("SELECT a FROM Agendamento a where a.nutricionista = "+idNutricionista +" and a.paciente = "+idPaciente).getResultList();
	}

}
