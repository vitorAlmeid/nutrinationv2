package br.com.nutrination.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.nutrination.entities.Dieta;
import br.com.nutrination.utils.ConnectionFactory;

public class DietaRepository extends GenericRepository<Dieta> {
	
	private static EntityManager entityManager = ConnectionFactory.getEntityManager();

	@SuppressWarnings("unchecked")
	public List<Dieta> buscarTodos(){
		entityManager = ConnectionFactory.getEntityManager();
		
		List<Dieta> list = entityManager.createQuery("SELECT d FROM Dieta d, Refeicao r, AlimentoRefeicao ar, Alimento a WHERE d.idDieta = r.dieta AND ar.refeicao = r.idRefeicao AND ar.alimento = a.idAlimento GROUP BY d.idDieta").getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Dieta> buscarTodosPorIdPacienteAndIdNutricionista(Long idNutricionista, Long idPaciente){
		entityManager = ConnectionFactory.getEntityManager();
		Query query = entityManager.createQuery("SELECT d FROM Dieta d , Refeicao r, AlimentoRefeicao ar, Alimento a WHERE d.idDieta = r.dieta AND ar.refeicao = r.idRefeicao AND ar.alimento = a.idAlimento AND d.idNutricionista = :idNutricionista AND d.idPaciente = :idPaciente");
		query.setParameter("idNutricionista", idNutricionista);
		query.setParameter("idPaciente", idPaciente);
 
		return query.getResultList();
	}
	
	public Dieta buscarUltimaPaciente(Long idPaciente){
		entityManager = ConnectionFactory.getEntityManager();
		Query query = entityManager.createQuery("SELECT d FROM Dieta d , Refeicao r, AlimentoRefeicao ar, Alimento a WHERE d.idDieta = r.dieta AND ar.refeicao = r.idRefeicao AND ar.alimento = a.idAlimento AND d.idPaciente like ?1");
		query.setParameter(1, idPaciente);
		return (Dieta) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Dieta> buscarTodosPorIdNutricionista(Long idNutricionista){
		entityManager = ConnectionFactory.getEntityManager();
		Query query = entityManager.createQuery("SELECT d FROM Dieta d , Refeicao r, AlimentoRefeicao ar, Alimento a WHERE d.idDieta = r.dieta AND ar.refeicao = r.idRefeicao AND ar.alimento = a.idAlimento AND d.idNutricionista = :idNutricionista");
		query.setParameter("idNutricionista", idNutricionista);
 
		return query.getResultList();
	}
}