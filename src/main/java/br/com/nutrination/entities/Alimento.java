/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nutrination.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "alimento")
public class Alimento implements Serializable, GenericEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAlimento;
    
    @Column(nullable = false)
    private String nome;
    
    @Column(nullable = false)
    private String tipoAlimento;
    
    //Macro Nutrientes
    @Column(nullable = false)
    private Double proteinaPorCem;
    @Column(nullable = false)
    private Double carboidratoPorCem;
    @Column(nullable = false)
    private Double gorduraPorCem;
    
    //Micro Nutrientes
    @Column(nullable = true)
    private Double energia;
    @Column(nullable = true)
    private Double fibra;
    @Column(nullable = true)
    private Double fosforo;
    @Column(nullable = true)
    private Double potassio;
    @Column(nullable = true)
    private Double calcio;
    @Column(nullable = true)
    private Double magnesio;
    @Column(nullable = true)
    private Double cobre;
    @Column(nullable = true)
    private Double ferro;
    @Column(nullable = true)
    private Double manganes;
    @Column(nullable = true)
    private Double zinco;

    
	public Long getIdAlimento() {
		return idAlimento;
	}
	public void setIdAlimento(Long idAlimento) {
		this.idAlimento = idAlimento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getProteinaPorCem() {
		return proteinaPorCem;
	}
	public void setProteinaPorCem(Double proteinaPorCem) {
		this.proteinaPorCem = proteinaPorCem;
	}
	public Double getCarboidratoPorCem() {
		return carboidratoPorCem;
	}
	public void setCarboidratoPorCem(Double carboidratoPorCem) {
		this.carboidratoPorCem = carboidratoPorCem;
	}
	public Double getGorduraPorCem() {
		return gorduraPorCem;
	}
	public void setGorduraPorCem(Double gorduraPorCem) {
		this.gorduraPorCem = gorduraPorCem;
	}

	public Double getFosforo() {
		return fosforo;
	}
	public void setFosforo(Double fosforo) {
		this.fosforo = fosforo;
	}
	public Double getPotassio() {
		return potassio;
	}
	public void setPotassio(Double potassio) {
		this.potassio = potassio;
	}
	public Double getCalcio() {
		return calcio;
	}
	public void setCalcio(Double calcio) {
		this.calcio = calcio;
	}
	public Double getMagnesio() {
		return magnesio;
	}
	public void setMagnesio(Double magnesio) {
		this.magnesio = magnesio;
	}
	public Double getCobre() {
		return cobre;
	}
	public void setCobre(Double cobre) {
		this.cobre = cobre;
	}
	public Double getFerro() {
		return ferro;
	}
	public void setFerro(Double ferro) {
		this.ferro = ferro;
	}
	public Double getManganes() {
		return manganes;
	}
	public void setManganes(Double manganes) {
		this.manganes = manganes;
	}
	public Double getZinco() {
		return zinco;
	}
	public void setZinco(Double zinco) {
		this.zinco = zinco;
	}
	public Double getFibra() {
		return fibra;
	}
	public void setFibra(Double fibra) {
		this.fibra = fibra;
	}
	
	public String getTipoAlimento() {
		return tipoAlimento;
	}
	public void setTipoAlimento(String tipoAlimento) {
		this.tipoAlimento = tipoAlimento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAlimento == null) ? 0 : idAlimento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alimento other = (Alimento) obj;
		if (idAlimento == null) {
			if (other.idAlimento != null)
				return false;
		} else if (!idAlimento.equals(other.idAlimento))
			return false;
		return true;
	}
	@Override
	public Long getId() {
		return idAlimento;
	}
    
}
