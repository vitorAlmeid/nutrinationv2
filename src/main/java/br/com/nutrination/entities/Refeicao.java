/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nutrination.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "refeicao")
public class Refeicao implements Serializable, GenericEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idRefeicao;
    
    @Column
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Dieta dieta;
    
    @OneToMany
    (mappedBy="refeicao", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<AlimentoRefeicao> alimentos = new ArrayList<>();


	public List<AlimentoRefeicao> getAlimentos() {
		return alimentos;
	}

	public void setAlimentos(List<AlimentoRefeicao> alimentos) {
		this.alimentos = alimentos;
	}

	public Dieta getDieta() {
		return dieta;
	}

	public void setDieta(Dieta dieta) {
		this.dieta = dieta;
	}

	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	public Long getIdRefeicao() {
		return idRefeicao;
	}

	public void setIdRefeicao(Long idRefeicao) {
		this.idRefeicao = idRefeicao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRefeicao == null) ? 0 : idRefeicao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Refeicao other = (Refeicao) obj;
		if (idRefeicao == null) {
			if (other.idRefeicao != null)
				return false;
		} else if (!idRefeicao.equals(other.idRefeicao))
			return false;
		return true;
	}

	@Override
	public Long getId() {
		return idRefeicao;
	}
}
