/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nutrination.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.nutrination.enums.PerfilEnum;


/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "usuario")
public class Usuario implements Serializable, GenericEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idUsuario;

	@Column(nullable = true)
	private String email;

	@Column(nullable = true)
	private String senha;

	@Enumerated(EnumType.STRING)
	@Column(nullable = true)
	private PerfilEnum permissao;

	@Column(nullable = true)
	private String nome;

	@Column(nullable = true)
	private String cpf;

	@Column(nullable = true)
	private String telefone;
	
	@Column(nullable = true)
	private String celular;
	
	@Column(nullable = true)
	private String rua;
	
	@Column(nullable = true)
	private String numero;
	
	@Column(nullable = true)
	private String cep;
	
	@Column(nullable = true)
	private String bairro;
	
	@Column(nullable = true)
	private String cidade;
	
	@Column(nullable = true)
	private String estado;
	
	@Column(nullable = true)
	private String crnEstado;
	
	@Column(nullable = true)
	private Long child;
	
	public Usuario() {
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public PerfilEnum getPermissao() {
		return permissao;
	}

	public void setPermissao(PerfilEnum permissao) {
		this.permissao = permissao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCrnEstado() {
		return crnEstado;
	}

	public void setCrnEstado(String crnEstado) {
		this.crnEstado = crnEstado;
	}
	
	public Long getChild() {
		return child;
	}

	public void setChild(Long child) {
		this.child = child;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idUsuario == null) ? 0 : idUsuario.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (idUsuario == null) {
			if (other.idUsuario != null)
				return false;
		} else if (!idUsuario.equals(other.idUsuario))
			return false;
		return true;
	}

	@Override
	public Long getId() {
		return idUsuario;
	}

}
