/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.nutrination.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "dieta")
public class Dieta implements Serializable, GenericEntity {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idDieta;
	
	@Column
    private String nome;

	@Basic
	private java.time.LocalDateTime localDateTime;
	
	@OneToMany(mappedBy = "dieta", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Refeicao> refeicoes;
	
	@Column
    private Long idNutricionista;
	@Column
    private Long idPaciente;



	public List<Refeicao> getRefeicoes() {
		return refeicoes;
	}

	public void setRefeicoes(List<Refeicao> refeicoes) {
		this.refeicoes = refeicoes;
	}

	public Long getIdNutricionista() {
		return idNutricionista;
	}

	public void setIdNutricionista(Long idNutricionista) {
		this.idNutricionista = idNutricionista;
	}

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public Long getIdDieta() {
		return idDieta;
	}

	public void setIdDieta(Long idDieta) {
		this.idDieta = idDieta;
	}

	public java.time.LocalDateTime getLocalDateTime() {
		return localDateTime;
	}

	public void setLocalDateTime(java.time.LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDieta == null) ? 0 : idDieta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dieta other = (Dieta) obj;
		if (idDieta == null) {
			if (other.idDieta != null)
				return false;
		} else if (!idDieta.equals(other.idDieta))
			return false;
		return true;
	}

	@Override
	public Long getId() {
		return idDieta;
	}

	
}
