package br.com.nutrination.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Avaliacao implements Serializable, GenericEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAvaliacao;	


	@Column
    private double peso;
    
    @Column
    private double altura;
    
    @Column
    private double gorduraCorporal;
    
    @Column
    private double imc;
    
    @Column
    private String data;
    
    @Column
    private String hora;
    
    @OneToOne
    private Usuario nutricionista;
    
    @OneToOne
    private Usuario paciente;
    
    public Avaliacao() {
    }
    
    
	public Avaliacao(double peso, double altura, double gorduraCorporal, double imc,
			Usuario nutricionista, Usuario paciente, String data, String hora) {
		super();
		this.peso = peso;
		this.altura = altura;
		this.gorduraCorporal = gorduraCorporal;
		this.imc = imc;
		this.nutricionista = nutricionista;
		this.paciente = paciente;
		this.data = data;
		this.hora = hora;
	}


	@Override
	public Long getId() {
		return idAvaliacao;
	}

	public Long getIdAvaliacao() {
		return idAvaliacao;
	}

	public void setIdAvaliacao(Long idAvaliacao) {
		this.idAvaliacao = idAvaliacao;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getGorduraCorporal() {
		return gorduraCorporal;
	}

	public void setGorduraCorporal(double gorduraCorporal) {
		this.gorduraCorporal = gorduraCorporal;
	}

	public double getImc() {
		return imc;
	}

	public void setImc(double imc) {
		this.imc = imc;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}


	public String getHora() {
		return hora;
	}


	public void setHora(String hora) {
		this.hora = hora;
	}


	public Usuario getNutricionista() {
		return nutricionista;
	}

	public void setNutricionista(Usuario nutricionista) {
		this.nutricionista = nutricionista;
	}

	public Usuario getPaciente() {
		return paciente;
	}

	public void setPaciente(Usuario paciente) {
		this.paciente = paciente;
	}

}
